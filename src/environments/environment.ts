// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    url: 'http://localhost/angular/slim/',
    firebase:{
    apiKey: "AIzaSyANLfOtBa_WX1GxBgs-KGqKl929_O5r6-Y",
    authDomain: "products-ea01f.firebaseapp.com",
    databaseURL: "https://products-ea01f.firebaseio.com",
    projectId: "products-ea01f",
    storageBucket: "products-ea01f.appspot.com",
    messagingSenderId: "45020814715"
  }
};
