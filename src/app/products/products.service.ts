import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductsService {

  // Variables
  http:Http;

  getProducts(){  
	   return this.http.get(environment.url + '/products');
  }

  searchProducts(name){
    let params = new HttpParams().append('name', name);
    let options =  {
        headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'     
        })
    } 
    return this.http.post(environment.url + '/products/search', params.toString(), options); 
  }

  getProduct(key){  
     return this.http.get(environment.url + '/products/' + key);
  }

  updateProduct(data){       
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })   
    };

    let params = new HttpParams().append('name',data.name).append('price',data.price);  

    return this.http.put(environment.url + '/products/' + data.id,params.toString(), options);      
  }

  getProductsFireBase(){
    return this.db.list('/products').valueChanges();
  }

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}
