import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  // Instace Variables
  search_term;
  products;
  productsKeys;
  length_keys;
  service:ProductsService;

  constructor(service:ProductsService, private route: ActivatedRoute, private router: Router) {
     this.service = service;    
     this.route.paramMap.subscribe((params: ParamMap) => {
        this.search_term = params.get('search_term');
        this.service.searchProducts(this.search_term).subscribe(response=>{
          this.products = response.json();
          this.productsKeys = Object.keys(this.products);
          this.length_keys = this.productsKeys.length;                           
        });      
    });
  }

  ngOnInit() {
  }

}
