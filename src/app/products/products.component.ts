import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductsService } from './products.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  // Variables
  products;
  productsKeys;
  length_keys;
  
  // Instance Variables
  product_service:ProductsService;

  //Form Builder
  searchForm = new FormGroup({
      name:new FormControl()
  }); 

  sendData(){
    this.router.navigate(['/search-results/' + this.searchForm.value.name]);
  }

  constructor(private service:ProductsService, private router:Router, private formBuilder:FormBuilder) {
    // Overload
    this.product_service = service;
//go to the service
    service.getProducts().subscribe(response=>{      
    	this.products = response.json();
      this.productsKeys = Object.keys(this.products);
      this.length_keys = this.productsKeys.length;
      console.log(this.productsKeys)
 	  });
  }

  ngOnInit() {
  }

}
